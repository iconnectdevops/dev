<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output indent="yes"/>
    <xsl:template match="/">

        <message>
            <xsl:variable name="date"
                select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:InvoiceDate/*:GLogDate"/>
            <xsl:if
                test="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:ConsolidationType = 'STANDARD'">

                <bill>

                    <id>
                        <xsl:attribute name="appCode">
                            <xsl:value-of
                                select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:InvoiceGid/*:Gid/*:DomainName"
                            />
                        </xsl:attribute>
                        <xsl:value-of
                            select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:InvoiceGid/*:Gid/*:Xid"
                        />
                    </id>
                    <creationDate>
                        <xsl:if test="$date">
                            <xsl:variable name="year" select="substring($date, 0, 5)"/>
                            <xsl:variable name="mes" select="substring($date, 5, 2)"/>
                            <xsl:variable name="dia" select="substring($date, 7, 2)"/>
                            <xsl:variable name="hora" select="substring($date, 9, 2)"/>
                            <xsl:variable name="minuto" select="substring($date, 11, 2)"/>
                            <xsl:variable name="seg" select="'00'"/>


                            <xsl:variable name="newDate"
                                select="concat($year, '-', $mes, '-', $dia)"/>

                            <xsl:variable name="tiemp"
                                select="concat($hora, ':', $minuto, ':', $seg)"/>

                            <xsl:variable name="timeStamp"
                                select="dateTime(xs:date($newDate), xs:time($tiemp))"/>
                            <xsl:variable name="fecha2" as="xs:string"
                                select="format-dateTime($timeStamp, '[D01]/[M01]/[Y0001] [H01]:[m01]:[s01]')"/>
                            <xsl:value-of select="$fecha2"/>
                        </xsl:if>

                    </creationDate>
                    <approvedDate/>
                    <approvedBy/>
                    <capturedBy/>
                    <consolidationType>
                        <xsl:value-of
                            select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:ConsolidationType"
                        />
                    </consolidationType>
                    <travel>
                        <id type="" appCode="">
                            <xsl:value-of
                                select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Shipment/*:ShipmentHeader/*:ShipmentGid/*:Gid/*:Xid"
                            />
                        </id>
                        <startDate/>
                        <endDate/>
                        <locations>
                            <location type="">
                                <id type="" typeName="" appCode=""/>
                                <description/>
                                <transactionCode/>
                                <status>
                                    <id/>
                                    <name/>
                                </status>
                                <isTemporary/>
                                <isTemplate/>
                                <rol>
                                    <id/>
                                    <description/>
                                </rol>
                                <address>
                                    <id/>
                                    <street/>
                                    <interiorNumber/>
                                    <exteriorNumber/>
                                    <postalCode/>
                                    <municipality/>
                                    <population/>
                                    <neighborhood/>
                                    <locality/>
                                    <entity/>
                                    <state>
                                        <id/>
                                        <name/>
                                    </state>
                                    <city>
                                        <id/>
                                        <name/>
                                    </city>
                                    <country>
                                        <id/>
                                        <name/>
                                        <code/>
                                    </country>
                                    <timeZone id=""/>
                                    <latitude/>
                                    <allAddress/>
                                    <longitude/>
                                </address>
                            </location>
                            <location type="">
                                <id type="" typeName="" appCode=""/>
                                <description/>
                                <transactionCode/>
                                <status>
                                    <id/>
                                    <name/>
                                </status>
                                <isTemporary/>
                                <isTemplate/>
                                <rol>
                                    <id/>
                                    <description/>
                                </rol>
                                <address>
                                    <id/>
                                    <street/>
                                    <interiorNumber/>
                                    <exteriorNumber/>
                                    <postalCode/>
                                    <municipality/>
                                    <population/>
                                    <neighborhood/>
                                    <locality/>
                                    <entity/>
                                    <state>
                                        <id/>
                                        <name/>
                                    </state>
                                    <city>
                                        <id/>
                                        <name/>
                                    </city>
                                    <country>
                                        <id/>
                                        <name/>
                                        <code/>
                                    </country>
                                    <timeZone id=""/>
                                    <latitude/>
                                    <allAddress/>
                                    <longitude/>
                                </address>
                            </location>
                        </locations>
                        <stops>
                            <Stop>
                                <nameStop/>
                                <numberStop/>
                            </Stop>
                        </stops>
                        <hours/>
                        <transportation>
                            <truck>
                                <id type="" typeName=""/>
                                <mileage/>
                                <enrollment/>
                                <value/>
                                <status id="" appCode="">
                                    <id/>
                                    <type/>
                                    <name/>
                                    <value/>
                                </status>
                                <equipments>
                                    <Equipment>
                                        <id type=""/>
                                        <code/>
                                        <name/>
                                        <cost/>
                                        <status/>
                                    </Equipment>
                                </equipments>
                            </truck>
                        </transportation>
                    </travel>
                    <cedis>
                        <id appCode="" name=""/>
                        <name/>
                        <stateCode/>
                    </cedis>
                    <drivers>
                        <driver type="">
                            <id/>
                            <name/>
                            <status/>
                            <pay type=""/>
                            <license>
                                <folio/>
                                <expiration/>
                                <status/>
                            </license>

                            <bankAccount>
                                <id/>
                                <name/>
                                <numberAccount/>
                                <interbankAccount/>
                            </bankAccount>


                        </driver>
                        <driver type="">
                            <id/>
                            <name/>
                            <status/>
                            <pay type=""/>
                            <license>
                                <folio/>
                                <expiration/>
                                <status/>
                            </license>

                            <bankAccount>
                                <id/>
                                <name/>
                                <numberAccount/>
                                <interbankAccount/>
                            </bankAccount>

                        </driver>
                    </drivers>
                    <folio/>
                    <lineItems>
                        <xsl:for-each
                            select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem">
                            <xsl:variable name="line" select="position()"/>
                            <lineItem>
                                <sequence>
                                    <xsl:value-of
                                        select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:AssignedNum"
                                    />
                                </sequence>
                                <typeCost/>
                                <accesory>
                                    <id type=""/>
                                    <name/>
                                    <description>
                                        <xsl:value-of
                                            select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode"
                                        />
                                    </description>
                                    <code>

                                        <xsl:value-of
                                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeDesc"
                                        />
                                    </code>
                                    <cost>
                                        <xsl:value-of
                                            select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:CommonInvoiceLineElements/*:FreightRate/*:FreightCharge/*:FinancialAmount/*:MonetaryAmount"
                                        />
                                    </cost>
                                    <generalLedger>
                                        <id/>
                                        <name/>
                                        <code>
                                            <xsl:value-of
                                                select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:CommonInvoiceLineElements/*:GeneralLedgerGid/*:Gid/*:Xid"
                                            />
                                        </code>
                                        <description/>
                                    </generalLedger>
                                </accesory>
                            </lineItem>
                        </xsl:for-each>
                    </lineItems>
                    <customer>
                        <recordControl>
                            <creationDate/>
                            <modificationDate/>
                            <masterCreationDate/>
                            <masterModificationDate/>
                        </recordControl>
                        <masterID/>
                        <id type="company" appCode=""/>
                        <transactionCode/>

                        <lastName/>
                        <firstName/>

                        <businessName/>
                        <fiscalName/>

                        <clasificationID/>
                        <thirdCustomerType/>
                        <xsl:for-each
                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Shipment/*:Location">
                            <xsl:variable name="pos" select="position()"/>
                            <xsl:if
                                test="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Shipment/*:Location[$pos]/*:LocationRefnum/*:LocationRefnumQualifierGid/*:Gid/*:Xid">
                                <rfc>
                                    <xsl:value-of
                                        select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Shipment/*:Location[$pos]/*:Corporation/*:CorporationGid/*:Gid/*:Xid"
                                    />
                                </rfc>
                            </xsl:if>
                        </xsl:for-each>


                        <description/>
                        <type id=""/>
                        <siteURL/>
                        <salesAgent>
                            <id/>
                            <name/>
                        </salesAgent>
                        <status>
                            <id/>
                            <name/>
                        </status>
                        <contacts>
                            <contact>
                                <id type="" appCode=""/>
                                <firstContact/>
                                <firstName/>
                                <lastName/>
                                <name/>
                                <phone/>
                                <status id="" appCode="">
                                    <id/>
                                    <type/>
                                    <name/>
                                    <value/>
                                </status>
                                <officePhone/>
                                <isNotification/>
                                <comunicationMethod/>

                                <codeMethod/>

                                <mail/>
                                <fax/>

                                <language>

                                    <id/>

                                    <name/>

                                </language>

                            </contact>
                        </contacts>
                        <comments>

                            <id/>

                            <text/>

                        </comments>

                        <locations>
                            <location>
                                <id type="" typeName="" appCode=""/>
                                <description/>
                                <transactionCode>NP</transactionCode>

                                <status>
                                    <id/>
                                    <name/>
                                </status>
                                <isTemporary/>

                                <isTemplate/>

                                <rol>
                                    <id/>
                                    <description/>
                                </rol>
                                <address>
                                    <id/>
                                    <street/>
                                    <interiorNumber/>
                                    <exteriorNumber/>
                                    <postalCode/>
                                    <municipality>
                                        <id/>
                                        <name/>
                                        <code/>
                                    </municipality>
                                    <population>
                                        <id/>
                                        <name/>
                                        <code/>
                                    </population>
                                    <neighborhood/>
                                    <locality>
                                        <id/>
                                        <name/>
                                        <code/>
                                    </locality>
                                    <entity/>
                                    <state>
                                        <id/>
                                        <name/>
                                        <code/>
                                    </state>
                                    <city>
                                        <id/>
                                        <name/>
                                    </city>
                                    <country>
                                        <id/>
                                        <name/>
                                        <code/>

                                    </country>
                                    <timeZone id=""/>

                                    <latitude/>

                                    <longitude> </longitude>

                                    <allAddress/>
                                </address>
                                <contacts>
                                    <contact>
                                        <id type="" appCode=""/>
                                        <firstContact/>
                                        <firstName/>
                                        <lastName/>
                                        <name/>
                                        <phone/>
                                        <status id="" appCode="">
                                            <id/>
                                            <type/>
                                            <name/>
                                            <value/>
                                        </status>
                                        <officePhone/>
                                        <isNotification/>
                                        <comunicationMethod/>

                                        <codeMethod1/>

                                        <mail/>
                                        <fax/>

                                        <language>

                                            <id/>

                                            <name/>

                                        </language>

                                    </contact>
                                </contacts>
                            </location>
                        </locations>
                        <paymentConfig>
                            <payment>
                                <paymentWay>
                                    <id/>
                                    <name/>
                                </paymentWay>
                                <paymentMethod>
                                    <id/>
                                    <name/>
                                </paymentMethod>
                                <cfdiUse>
                                    <id/>
                                    <name/>
                                </cfdiUse>
                                <paymentDeadline>
                                    <id/>
                                    <name/>
                                </paymentDeadline>
                            </payment>

                        </paymentConfig>
                    </customer>
                    <currency>
                        <id/>
                        <name/>
                        <code>
                            <xsl:value-of
                                select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:NetAmountDue/*:FinancialAmount/*:GlobalCurrencyCode"
                            />
                        </code>
                        <value/>
                    </currency>
                    <paymentConfig>
                        <payment>
                            <folio/>
                            <amountOfPay>
                                <xsl:value-of
                                    select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:NetAmountDue/*:FinancialAmount/*:MonetaryAmount"
                                />
                            </amountOfPay>
                            <paymentWay>
                                <id/>
                                <name/>
                            </paymentWay>
                            <paymentMethod>
                                <id/>
                                <name/>
                            </paymentMethod>
                            <cfdiUse>
                                <id/>
                                <name/>
                            </cfdiUse>
                            <paymentDeadline>
                                <id/>
                                <name/>
                                <value/>
                            </paymentDeadline>
                        </payment>
                    </paymentConfig>
                </bill>
            </xsl:if>
            <xsl:if
                test="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:ConsolidationType = 'PARENT'">
                <bill xmlns:otm="http://xmlns.oracle.com/apps/otm/transmission/v6.4"
                    xmlns:gtm="http://xmlns.oracle.com/apps/gtm/transmission/v6.4">
                    <id>
                        <xsl:attribute name="appCode">
                            <xsl:value-of
                                select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:InvoiceGid/*:Gid/*:DomainName"
                            />
                        </xsl:attribute>
                        <xsl:value-of
                            select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:InvoiceGid/*:Gid/*:Xid"
                        />
                    </id>
                    <creationDate>
                        <xsl:variable name="creation"
                            select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:InvoiceDate/*:GLogDate"/>
                        <xsl:if test="$creation">
                            <xsl:variable name="year" select="substring($date, 0, 5)"/>
                            <xsl:variable name="mes" select="substring($date, 5, 2)"/>
                            <xsl:variable name="dia" select="substring($date, 7, 2)"/>
                            <xsl:variable name="hora" select="substring($date, 9, 2)"/>
                            <xsl:variable name="minuto" select="substring($date, 11, 2)"/>
                            <xsl:variable name="seg" select="'00'"/>

                            <xsl:variable name="newDate"
                                select="concat($year, '-', $mes, '-', $dia)"/>

                            <xsl:variable name="tiemp"
                                select="concat($hora, ':', $minuto, ':', $seg)"/>

                            <xsl:variable name="timeStamp"
                                select="dateTime(xs:date($newDate), xs:time($tiemp))"/>
                            <xsl:variable name="fecha2" as="xs:string"
                                select="format-dateTime($timeStamp, '[D01]/[M01]/[Y0001] [H01]:[m01]:[s01]')"/>
                            <xsl:value-of select="$fecha2"/>
                        </xsl:if>
                    </creationDate>
                    <approvedDate/>
                    <approvedBy/>
                    <capturedBy/>
                    <consolidationType>
                        <xsl:value-of
                            select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:ConsolidationType"
                        />
                    </consolidationType>
                    <travel>
                        <id type="" appCode="">
                            <xsl:value-of
                                select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Shipment/*:ShipmentHeader/*:ShipmentGid/*:Gid/*:Xid"
                            />
                        </id>
                        <startDate/>
                        <endDate/>
                        <locations>
                            <location type="">
                                <id type="" typeName="" appCode=""/>
                                <description/>
                                <transactionCode/>
                                <status>
                                    <id/>
                                    <name/>
                                </status>
                                <isTemporary/>
                                <isTemplate/>
                                <rol>
                                    <id/>
                                    <description/>
                                </rol>
                                <address>
                                    <id/>
                                    <street/>
                                    <interiorNumber/>
                                    <exteriorNumber/>
                                    <postalCode/>
                                    <municipality/>
                                    <population/>
                                    <neighborhood/>
                                    <locality/>
                                    <entity/>
                                    <state>
                                        <id/>
                                        <name/>
                                    </state>
                                    <city>
                                        <id/>
                                        <name/>
                                    </city>
                                    <country>
                                        <id/>
                                        <name/>
                                        <code/>
                                    </country>
                                    <timeZone id=""/>
                                    <latitude/>
                                    <allAddress/>
                                    <longitude/>
                                </address>
                            </location>
                            <location type="">
                                <id type="" typeName="" appCode=""/>
                                <description/>
                                <transactionCode/>
                                <status>
                                    <id/>
                                    <name/>
                                </status>
                                <isTemporary/>
                                <isTemplate/>
                                <rol>
                                    <id/>
                                    <description/>
                                </rol>
                                <address>
                                    <id/>
                                    <street/>
                                    <interiorNumber/>
                                    <exteriorNumber/>
                                    <postalCode/>
                                    <municipality/>
                                    <population/>
                                    <neighborhood/>
                                    <locality/>
                                    <entity/>
                                    <state>
                                        <id/>
                                        <name/>
                                    </state>
                                    <city>
                                        <id/>
                                        <name/>
                                    </city>
                                    <country>
                                        <id/>
                                        <name/>
                                        <code/>
                                    </country>
                                    <timeZone id=""/>
                                    <latitude/>
                                    <allAddress/>
                                    <longitude/>
                                </address>
                            </location>
                        </locations>
                        <stops>
                            <Stop>
                                <nameStop/>
                                <numberStop/>
                            </Stop>
                        </stops>
                        <hours/>
                        <transportation>
                            <truck>
                                <id type="" typeName=""/>
                                <mileage/>
                                <enrollment/>
                                <value/>
                                <status id="" appCode="">
                                    <id/>
                                    <type/>
                                    <name/>
                                    <value/>
                                </status>
                                <equipments>
                                    <Equipment>
                                        <id type=""/>
                                        <code/>
                                        <name/>
                                        <cost/>
                                        <status/>
                                    </Equipment>
                                </equipments>
                            </truck>
                        </transportation>
                    </travel>
                    <cedis>
                        <id appCode="" name=""/>
                        <name/>
                        <stateCode/>
                    </cedis>
                    <drivers>
                        <driver type="">
                            <id/>
                            <name/>
                            <status/>
                            <pay type=""/>
                            <license>
                                <folio/>
                                <expiration/>
                                <status/>
                            </license>
                            <bankAccount>
                                <id/>
                                <name/>
                                <numberAccount/>
                                <interbankAccount/>
                            </bankAccount>
                        </driver>
                        <driver type="">
                            <id/>
                            <name/>
                            <status/>
                            <pay type=""/>
                            <license>
                                <folio/>
                                <expiration/>
                                <status/>
                            </license>

                            <bankAccount>
                                <id/>
                                <name/>
                                <numberAccount/>
                                <interbankAccount/>
                            </bankAccount>

                        </driver>
                    </drivers>
                    <folio/>
                    <lineItems>
                        <xsl:for-each
                            select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem">
                            <xsl:variable name="line" select="position()"/>
                            <lineItem>
                                <sequence>
                                    <xsl:value-of
                                        select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:AssignedNum"
                                    />
                                </sequence>
                                <typeCost/>
                                <accesory>
                                    <id type=""/>
                                    <name/>
                                    <description>
                                        <xsl:value-of
                                            select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode"
                                        />
                                    </description>
                                    <code>
                                        <xsl:value-of
                                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeDesc"
                                        />
                                    </code>
                                    <cost>
                                        <xsl:value-of
                                            select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:CommonInvoiceLineElements/*:FreightRate/*:FreightCharge/*:FinancialAmount/*:MonetaryAmount"
                                        />
                                    </cost>
                                    <generalLedger>
                                        <id/>
                                        <name/>
                                        <code>
                                            <xsl:value-of
                                                select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$line]/*:CommonInvoiceLineElements/*:GeneralLedgerGid/*:Gid/*:Xid"
                                            />
                                        </code>
                                        <description/>
                                    </generalLedger>
                                </accesory>
                            </lineItem>
                        </xsl:for-each>
                    </lineItems>
                    <customer>
                        <recordControl>
                            <creationDate/>
                            <modificationDate/>
                            <masterCreationDate/>
                            <masterModificationDate/>
                        </recordControl>
                        <masterID/>
                        <id type="" appCode=""/>
                        <transactionCode/>

                        <lastName/>
                        <firstName/>

                        <businessName/>
                        <fiscalName/>

                        <clasificationID/>
                        <thirdCustomerType/>
                        <xsl:for-each
                            select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Shipment/*:Location">
                            <xsl:variable name="pos" select="position()"/>
                            <xsl:if
                                test="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Shipment/*:Location[$pos]/*:LocationRefnum/*:LocationRefnumQualifierGid/*:Gid/*:Xid">
                                <rfc>
                                    <xsl:value-of
                                        select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Shipment/*:Location[$pos]/*:Corporation/*:CorporationGid/*:Gid/*:Xid"
                                    />
                                </rfc>
                            </xsl:if>
                        </xsl:for-each>
                        <description/>
                        <type id=""/>
                        <siteURL/>
                        <salesAgent>
                            <id/>
                            <name/>
                        </salesAgent>
                        <status>
                            <id/>
                            <name/>
                        </status>
                        <contacts>
                            <contact>
                                <id type="" appCode=""/>
                                <firstContact/>
                                <firstName/>
                                <lastName/>
                                <name/>
                                <phone/>
                                <status id="" appCode="">
                                    <id/>
                                    <type/>
                                    <name/>
                                    <value/>
                                </status>
                                <officePhone/>
                                <isNotification/>
                                <comunicationMethod/>

                                <codeMethod/>

                                <mail/>
                                <fax/>

                                <language>

                                    <id/>

                                    <name/>

                                </language>

                            </contact>
                        </contacts>
                        <comments>

                            <id/>

                            <text/>

                        </comments>

                        <locations>
                            <location>
                                <id type="" typeName="" appCode=""/>
                                <description/>
                                <transactionCode/>

                                <status>
                                    <id/>
                                    <name/>
                                </status>
                                <isTemporary/>

                                <isTemplate/>

                                <rol>
                                    <id/>
                                    <description/>
                                </rol>
                                <address>
                                    <id/>
                                    <street/>
                                    <interiorNumber/>
                                    <exteriorNumber/>
                                    <postalCode/>
                                    <municipality>
                                        <id/>
                                        <name/>
                                        <code/>
                                    </municipality>
                                    <population>
                                        <id/>
                                        <name/>
                                        <code/>
                                    </population>
                                    <neighborhood/>
                                    <locality>
                                        <id/>
                                        <name/>
                                        <code/>
                                    </locality>
                                    <entity/>
                                    <state>
                                        <id/>
                                        <name/>
                                        <code/>
                                    </state>
                                    <city>
                                        <id/>
                                        <name/>
                                    </city>
                                    <country>
                                        <id/>
                                        <name/>
                                        <code/>

                                    </country>
                                    <timeZone id=""/>

                                    <latitude/>

                                    <longitude> </longitude>

                                    <allAddress/>
                                </address>
                                <contacts>
                                    <contact>
                                        <id type="" appCode=""/>
                                        <firstContact/>
                                        <firstName/>
                                        <lastName/>
                                        <name/>
                                        <phone/>
                                        <status id="" appCode="">
                                            <id/>
                                            <type/>
                                            <name/>
                                            <value/>
                                        </status>
                                        <officePhone/>
                                        <isNotification/>
                                        <comunicationMethod/>

                                        <codeMethod1/>

                                        <mail/>
                                        <fax/>

                                        <language>

                                            <id/>

                                            <name/>

                                        </language>

                                    </contact>
                                </contacts>
                            </location>
                        </locations>
                        <paymentConfig>
                            <payment>
                                <paymentWay>
                                    <id/>
                                    <name/>
                                </paymentWay>
                                <paymentMethod>
                                    <id/>
                                    <name/>
                                </paymentMethod>
                                <cfdiUse>
                                    <id/>
                                    <name/>
                                </cfdiUse>
                                <paymentDeadline>
                                    <id/>
                                    <name/>
                                </paymentDeadline>
                            </payment>

                        </paymentConfig>
                    </customer>
                    <currency>
                        <id/>
                        <name/>
                        <code/>
                        <value/>
                    </currency>
                    <paymentConfig>
                        <payment>
                            <folio/>
                            <amountOfPay>
                                <xsl:value-of
                                    select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:NetAmountDue/*:FinancialAmount/*:MonetaryAmount"
                                />
                            </amountOfPay>
                            <paymentWay>
                                <id/>
                                <name/>
                            </paymentWay>
                            <paymentMethod>
                                <id/>
                                <name/>
                            </paymentMethod>
                            <cfdiUse>
                                <id/>
                                <name/>
                            </cfdiUse>
                            <paymentDeadline>
                                <id/>
                                <name/>
                                <value/>
                            </paymentDeadline>
                        </payment>


                    </paymentConfig>
                    <childrenBill>
                        <xsl:for-each
                            select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing">
                            <xsl:variable name="pos" select="position()"/>
                            <bill>
                                <id>
                                    <xsl:attribute name="appCode">
                                        <xsl:value-of
                                            select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:InvoiceGid/*:Gid/*:DomainName"
                                        />
                                    </xsl:attribute>
                                    <xsl:value-of
                                        select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Payment/*:PaymentHeader/*:InvoiceGid/*:Gid/*:Xid"
                                    />
                                </id>
                                <creationDate>

                                    <xsl:variable name="creationD"
                                        select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:InvoiceDate/*:GLogDate"/>
                                    <xsl:if test="$creationD">
                                        <xsl:variable name="year" select="substring($date, 0, 5)"/>
                                        <xsl:variable name="mes" select="substring($date, 5, 2)"/>
                                        <xsl:variable name="dia" select="substring($date, 7, 2)"/>
                                        <xsl:variable name="hora" select="substring($date, 9, 2)"/>
                                        <xsl:variable name="minuto" select="substring($date, 11, 2)"/>
                                        <xsl:variable name="seg" select="'00'"/>

                                        <xsl:variable name="newDate"
                                            select="concat($year, '-', $mes, '-', $dia)"/>

                                        <xsl:variable name="tiemp"
                                            select="concat($hora, ':', $minuto, ':', $seg)"/>

                                        <xsl:variable name="timeStamp"
                                            select="dateTime(xs:date($newDate), xs:time($tiemp))"/>
                                        <xsl:variable name="fecha2" as="xs:string"
                                            select="format-dateTime($timeStamp, '[D01]/[M01]/[Y0001] [H01]:[m01]:[s01]')"/>
                                        <xsl:value-of select="$fecha2"/>
                                    </xsl:if>


                                </creationDate>
                                <approvedDate/>
                                <approvedBy/>
                                <capturedBy/>
                                <consolidationType>
                                    <xsl:value-of
                                        select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Payment/*:PaymentHeader/*:ConsolidationType"
                                    />
                                </consolidationType>
                                <travel>
                                    <id type="" appCode="">
                                        <xsl:value-of
                                            select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Shipment/*:ShipmentHeader/*:ShipmentGid/*:Gid/*:Xid"
                                        />
                                    </id>
                                    <startDate/>
                                    <endDate/>
                                    <locations>
                                        <location type="">
                                            <id type="" typeName="" appCode=""/>
                                            <description/>
                                            <transactionCode/>
                                            <status>
                                                <id/>
                                                <name/>
                                            </status>
                                            <isTemporary/>
                                            <isTemplate/>
                                            <rol>
                                                <id/>
                                                <description/>
                                            </rol>
                                            <address>
                                                <id/>
                                                <street/>
                                                <interiorNumber/>
                                                <exteriorNumber/>
                                                <postalCode/>
                                                <municipality/>
                                                <population/>
                                                <neighborhood/>
                                                <locality/>
                                                <entity/>
                                                <state>
                                                  <id/>
                                                  <name/>
                                                </state>
                                                <city>
                                                  <id/>
                                                  <name/>
                                                </city>
                                                <country>
                                                  <id/>
                                                  <name/>
                                                  <code/>
                                                </country>
                                                <timeZone id=""/>
                                                <latitude/>
                                                <allAddress/>
                                                <longitude/>
                                            </address>
                                        </location>
                                        <location type="">
                                            <id type="" typeName="" appCode=""/>
                                            <description/>
                                            <transactionCode/>
                                            <status>
                                                <id/>
                                                <name/>
                                            </status>
                                            <isTemporary/>
                                            <isTemplate/>
                                            <rol>
                                                <id/>
                                                <description/>
                                            </rol>
                                            <address>
                                                <id/>
                                                <street/>
                                                <interiorNumber/>
                                                <exteriorNumber/>
                                                <postalCode/>
                                                <municipality/>
                                                <population/>
                                                <neighborhood/>
                                                <locality/>
                                                <entity/>
                                                <state>
                                                  <id/>
                                                  <name/>
                                                </state>
                                                <city>
                                                  <id/>
                                                  <name/>
                                                </city>
                                                <country>
                                                  <id/>
                                                  <name/>
                                                  <code/>
                                                </country>
                                                <timeZone id=""/>
                                                <latitude/>
                                                <allAddress/>
                                                <longitude/>
                                            </address>
                                        </location>
                                    </locations>
                                    <stops>
                                        <Stop>
                                            <nameStop/>
                                            <numberStop/>
                                        </Stop>
                                    </stops>
                                    <hours/>
                                    <transportation>
                                        <truck>
                                            <id type="" typeName=""/>
                                            <mileage/>
                                            <enrollment/>
                                            <value/>
                                            <status id="12" appCode="">
                                                <id/>
                                                <type/>
                                                <name/>
                                                <value/>
                                            </status>
                                            <equipments>
                                                <Equipment>
                                                  <id type=""/>
                                                  <code/>
                                                  <name/>
                                                  <cost/>
                                                  <status/>
                                                </Equipment>
                                            </equipments>
                                        </truck>
                                    </transportation>
                                </travel>
                                <lineItems>
                                    <xsl:for-each
                                        select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem">
                                        <xsl:variable name="num" select="position()"/>
                                        <lineItem>
                                            <sequence>
                                                <xsl:value-of
                                                  select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$num]/*:AssignedNum"
                                                />
                                            </sequence>
                                            <accesory>
                                                <id/>
                                                <name/>
                                                <description>
                                                  <xsl:value-of
                                                  select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$num]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeCode"
                                                  />
                                                </description>
                                                <code>

                                                  <xsl:value-of
                                                  select="//*:Transmission/*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$num]/*:CommonInvoiceLineElements/*:FreightRate/*:SpecialCharge/*:SpecialChargeDesc"
                                                  />
                                                </code>
                                                <cost>

                                                  <xsl:value-of
                                                  select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$num]/*:CommonInvoiceLineElements/*:FreightRate/*:FreightCharge/*:FinancialAmount/*:MonetaryAmount"
                                                  />
                                                </cost>
                                                <generalLedger>
                                                  <id/>
                                                  <name/>
                                                  <code>
                                                  <xsl:value-of
                                                  select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Payment/*:PaymentModeDetail/*:GenericDetail/*:GenericLineItem[$num]/*:CommonInvoiceLineElements/*:GeneralLedgerGid/*:Gid/*:Xid"
                                                  />
                                                  </code>
                                                  <description/>
                                                </generalLedger>
                                            </accesory>
                                        </lineItem>
                                    </xsl:for-each>
                                </lineItems>



                                <customer>
                                    <xsl:for-each
                                        select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Shipment/*:Location">
                                        <xsl:variable name="pos" select="position()"/>
                                        <xsl:if
                                            test="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Shipment/*:Location[$pos]/*:LocationRefnum/*:LocationRefnumQualifierGid/*:Gid/*:Xid">
                                            <rfc>
                                                <xsl:value-of
                                                  select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:ChildPayments/*:Billing[$pos]/*:Shipment/*:Location[$pos]/*:Corporation/*:CorporationGid/*:Gid/*:Xid"
                                                />
                                            </rfc>
                                        </xsl:if>
                                    </xsl:for-each>

                                </customer>
                                <currency>
                                    <id/>
                                    <name/>
                                    <code>
                                        <xsl:value-of
                                            select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:NetAmountDue/*:FinancialAmount/*:GlobalCurrencyCode"
                                        />
                                    </code>
                                    <value/>
                                </currency>
                                <paymentConfig>
                                    <payment>
                                        <folio/>
                                        <amountOfPay>
                                            <xsl:value-of
                                                select="//*:Transmission//*:TransmissionBody/*:GLogXMLElement/*:Billing/*:Payment/*:PaymentHeader/*:NetAmountDue/*:FinancialAmount/*:MonetaryAmount"
                                            />
                                        </amountOfPay>
                                        <paymentWay>
                                            <id/>
                                            <name/>
                                        </paymentWay>
                                        <paymentMethod>
                                            <id/>
                                            <name/>
                                        </paymentMethod>
                                        <cfdiUse>
                                            <id/>
                                            <name/>
                                        </cfdiUse>
                                        <paymentDeadline>
                                            <id/>
                                            <name/>
                                            <value/>
                                        </paymentDeadline>
                                    </payment>

                                </paymentConfig>
                            </bill>
                        </xsl:for-each>
                    </childrenBill>
                </bill>
            </xsl:if>
        </message>

    </xsl:template>
</xsl:stylesheet>
