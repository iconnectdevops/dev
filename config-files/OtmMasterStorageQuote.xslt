<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">
    <xsl:output indent="yes"/>
    <xsl:template match="/">
        <message>
            <warehouseQuote>
                <id>
                    <xsl:value-of select="//quote/transaction/transactionID_t"/>

                </id>
                <startDate/>
                <finishDate>
                    <xsl:value-of select="//quote/transaction/fechaFinVigencia"/>
                </finishDate>
                <customer>
                    <recordControl>
                        <creationDate/>
                        <modificationDate/>
                        <masterCreationDate/>
                        <masterModificationDate/>
                    </recordControl>
                    <id type="" appCode="">
                        <xsl:value-of select="//quote/transaction/_customer_id"/>
                    </id>
                    <xsl:choose>
                        <xsl:when test="//quote/transaction/IDClienteMasterAux_t != 'null'">
                            <masterID>
                                <xsl:value-of select="//quote/transaction/IDClienteMasterAux_t"/>
                            </masterID>
                        </xsl:when>
                        <xsl:otherwise>
                            <masterID/>
                        </xsl:otherwise>
                    </xsl:choose>

                </customer>
                <lineTransactions>
                    <xsl:for-each select="//quote/transactionline/items">
                        <xsl:variable name="pos" select="position()"/>
                        <lineTransaction>
                            <sequence>
                                <xsl:value-of select="$pos"/>
                            </sequence>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/rotacion != 'null'">
                                    <rotationDays>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/rotacion"/>
                                    </rotationDays>
                                </xsl:when>
                                <xsl:otherwise>
                                    <rotationDays/>
                                </xsl:otherwise>
                            </xsl:choose>

                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/diferenciaAlturaPallet/value != 'null'">
                                    <costDiferencePalletHeight>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/diferenciaAlturaPallet/value"
                                        />
                                    </costDiferencePalletHeight>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/costoDiasRotacion/value != 'null'">
                                    <costRotationDays>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/costoDiasRotacion/value"
                                        />
                                    </costRotationDays>
                                </xsl:when>
                                <xsl:otherwise>
                                    <costRotationDays/>
                                </xsl:otherwise>
                            </xsl:choose>

                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/personalExtra != 'null'">
                                    <extraStaff>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/personalExtra"
                                        />
                                    </extraStaff>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/personalExtra != 'null'">
                                    <costExtraStaff>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/personalExtra"
                                        />
                                    </costExtraStaff>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/tercerTurno != 'null'">
                                    <thirdTurn>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/tercerTurno"
                                        />
                                    </thirdTurn>
                                </xsl:when>
                                <xsl:otherwise>
                                    <thirdTurn/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/costoTercerTurno/value != 'null'">
                                    <costThirdTurn>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/costoTercerTurno/value"
                                        />
                                    </costThirdTurn>
                                </xsl:when>
                                <xsl:otherwise>
                                    <costThirdTurn/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <costOfPicking/>


                            <exitPackingService/>
                            <termSalePrice/>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/costoPorEntrada/value != 'null'">
                                    <costByEntry>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/costoPorEntrada/value"
                                        />
                                    </costByEntry>
                                </xsl:when>
                                <xsl:otherwise>
                                    <costByEntry/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/costoPorSalida/value != 'null'">
                                    <costByExit>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/costoPorSalida/value"
                                        />
                                    </costByExit>
                                </xsl:when>
                                <xsl:otherwise>
                                    <costByExit/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/costoUbicacion/value != 'null'">
                                    <totalCostPallet>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/costoUbicacion/value"
                                        />
                                    </totalCostPallet>
                                </xsl:when>
                                <xsl:otherwise>
                                    <totalCostPallet/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/costoEntrada/value != 'null'">
                                    <extraCostByEntry>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/costoEntrada/value"
                                        />
                                    </extraCostByEntry>
                                </xsl:when>
                                <xsl:otherwise>
                                    <extraCostByEntry/>
                                </xsl:otherwise>
                            </xsl:choose>

                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/costoSalida/value != 'null'">
                                    <extraCostByExit>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/costoSalida/value"
                                        />
                                    </extraCostByExit>
                                </xsl:when>
                                <xsl:otherwise>
                                    <extraCostByExit/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/costoTotalAlmacenaje/value != 'null'">
                                    <totalCostStock>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/costoTotalAlmacenaje/value"
                                        />
                                    </totalCostStock>
                                </xsl:when>
                                <xsl:otherwise>
                                    <totalCostStock/>
                                </xsl:otherwise>
                            </xsl:choose>

                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/tarifa_I/value != 'null'">
                                    <installmentSalePrice>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/tarifa_I/value"
                                        />
                                    </installmentSalePrice>
                                </xsl:when>
                                <xsl:otherwise>
                                    <installmentSalePrice/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/ventaEntrada/value != 'null'">
                                    <totalSalesByEntryCost>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/ventaEntrada/value"
                                        />
                                    </totalSalesByEntryCost>
                                </xsl:when>
                                <xsl:otherwise>
                                    <totalSalesByEntryCost/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/descargaDeContenedorAGranel/value != 'null'">
                                    <bulkContainerDischarge>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/descargaDeContenedorAGranel/value"
                                        />
                                    </bulkContainerDischarge>
                                </xsl:when>
                                <xsl:otherwise>
                                    <bulkContainerDischarge/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/ventaTotalDescargaDeContenedorAGranel/value != 'null'">
                                    <totalSalesBulkContainerDschrg>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/ventaTotalDescargaDeContenedorAGranel/value"
                                        />
                                    </totalSalesBulkContainerDschrg>
                                </xsl:when>
                                <xsl:otherwise>
                                    <totalSalesBulkContainerDschrg/>
                                </xsl:otherwise>
                            </xsl:choose>

                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/ventaSalida/value != 'null'">
                                    <totalSalesByExitCost>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/ventaSalida/value"
                                        />
                                    </totalSalesByExitCost>
                                </xsl:when>
                                <xsl:otherwise>
                                    <totalSalesByExitCost/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/costoDeManiobraDeDescargaPorCaja/value != 'null'">
                                    <unloadMnverCost>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/costoDeManiobraDeDescargaPorCaja/value"
                                        />
                                    </unloadMnverCost>
                                </xsl:when>
                                <xsl:otherwise>
                                    <unloadMnverCost/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/ventaTotalCostoDeManiobraDeDescarga/value != 'null'">
                                    <totalSalesUnloadMnverCost>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/ventaTotalCostoDeManiobraDeDescarga/value"
                                        />
                                    </totalSalesUnloadMnverCost>
                                </xsl:when>
                                <xsl:otherwise>
                                    <totalSalesUnloadMnverCost/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/costoPorCajaSurtidaPicking/value != 'null'">
                                    <costByPicking>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/costoPorCajaSurtidaPicking/value"
                                        />
                                    </costByPicking>
                                </xsl:when>
                                <xsl:otherwise>
                                    <costByPicking/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/ventaTotalCostoPorCajaSurtida/value != 'null'">
                                    <totalSalesOfPickCost>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/ventaTotalCostoPorCajaSurtida/value"
                                        />
                                    </totalSalesOfPickCost>
                                </xsl:when>
                                <xsl:otherwise>
                                    <totalSalesOfPickCost/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/costoPorTarimaUtilizada/value != 'null'">
                                    <costPalletUsed>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/costoPorTarimaUtilizada/value"
                                        />
                                    </costPalletUsed>
                                </xsl:when>
                                <xsl:otherwise>
                                    <costPalletUsed/>
                                </xsl:otherwise>
                            </xsl:choose>

                            <xsl:choose>
                                <xsl:when
                                    test="/quote/transactionline/items[$pos]/ventaTotalCostoPorTarimaUtilizada/value != 'null'">
                                    <totalSalesPalletUsedCost>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/ventaTotalCostoPorTarimaUtilizada/value"
                                        />
                                    </totalSalesPalletUsedCost>
                                </xsl:when>
                                <xsl:otherwise>
                                    <totalSalesPalletUsedCost/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/entregaEnCedisOccidente/value != 'null'">
                                    <deliveryToCO>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/entregaEnCedisOccidente/value"
                                        />
                                    </deliveryToCO>
                                </xsl:when>
                                <xsl:otherwise>
                                    <deliveryToCO/>
                                </xsl:otherwise>
                            </xsl:choose>

                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/ventaTotalEntregaEnCedisOccidente/value != 'null'">
                                    <totalSalesDeliveryToCO>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/ventaTotalEntregaEnCedisOccidente/value"
                                        />
                                    </totalSalesDeliveryToCO>
                                </xsl:when>
                                <xsl:otherwise>
                                    <totalSalesDeliveryToCO/>
                                </xsl:otherwise>
                            </xsl:choose>

                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/precioTiempoExtra/value != 'null'">
                                    <extraTimePrice>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/precioTiempoExtra/value"
                                        />
                                    </extraTimePrice>
                                </xsl:when>
                                <xsl:otherwise>
                                    <extraTimePrice/>
                                </xsl:otherwise>
                            </xsl:choose>


                            <extitPackingService/>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/servicioInventarioTarima/value != 'null'">
                                    <palletInventoryService>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/servicioInventarioTarima/value"
                                        />
                                    </palletInventoryService>
                                </xsl:when>
                                <xsl:otherwise>
                                    <palletInventoryService/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transaction/servicioEmbalajeSalida != 'null'">
                                    <exitPackingPrice>
                                        <xsl:value-of
                                            select="//quote/transaction/servicioEmbalajeSalida"/>
                                    </exitPackingPrice>
                                </xsl:when>
                                <xsl:otherwise>
                                    <exitPackingPrice/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transaction/servicioPorCajaContada != 'null'">
                                    <boxCountService>
                                        <xsl:value-of
                                            select="//quote/transaction/servicioPorCajaContada"/>
                                    </boxCountService>
                                </xsl:when>
                                <xsl:otherwise>
                                    <boxCountService/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transaction/servicioPorRequerimiento != 'null'">
                                    <serviceByRequirement>
                                        <xsl:value-of
                                            select="//quote/transaction/servicioPorRequerimiento"/>
                                    </serviceByRequirement>
                                </xsl:when>
                            </xsl:choose>

                            <palletInventoryCost/>
                            <costBoxCount/>
                            <assortmentByPiece/>
                            <costRequirement/>
                            <reportOfStock/>
                            <xsl:choose>
                                <xsl:when
                                    test="//quote/transactionline/items[$pos]/cantidadDePallets != 'null'">
                                    <qtyPalletsQuoted>
                                        <xsl:value-of
                                            select="//quote/transactionline/items[$pos]/cantidadDePallets"
                                        />
                                    </qtyPalletsQuoted>
                                </xsl:when>
                            </xsl:choose>

                            <storage>
                                <xsl:choose>
                                    <xsl:when
                                        test="//quote/transaction/idAlmacnHidalgoOGDL != 'null'">
                                        <id>
                                            <xsl:value-of
                                                select="//quote/transaction/idAlmacnHidalgoOGDL"/>
                                        </id>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <id/>
                                    </xsl:otherwise>
                                </xsl:choose>

                                <correspondence/>
                                <totalCapacity>
                                    <xsl:choose>
                                        <xsl:when
                                            test="//quote/transactionline/items[$pos]/capacidadInstalada != 'null'">
                                            <xsl:value-of
                                                select="//quote/transactionline/items[$pos]/capacidadInstalada"
                                            />
                                        </xsl:when>
                                    </xsl:choose>
                                </totalCapacity>
                            </storage>
                            <equipmentStorage>
                                <id/>
                                <name/>
                                <xsl:choose>
                                    <xsl:when
                                        test="/quote/transactionline/items[$pos]/costoPorPallet/value != 'null'">
                                        <cost>
                                            <xsl:value-of
                                                select="/quote/transactionline/items[$pos]/costoPorPallet/value"
                                            />
                                        </cost>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <cost/>
                                    </xsl:otherwise>
                                </xsl:choose>

                                <status/>
                                <xsl:choose>
                                    <xsl:when
                                        test="//quote/transactionline/items[$pos]/alturaDePallet != 'null'">
                                        <heightPallet>
                                            <xsl:value-of
                                                select="//quote/transactionline/items[$pos]/alturaDePallet"
                                            />
                                        </heightPallet>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <heightPallet/>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <xsl:choose>
                                    <xsl:when test="//quote/transaction/pesoTarima != 'null'">
                                        <weight>
                                            <xsl:value-of select="//quote/transaction/pesoTarima"/>
                                        </weight>

                                    </xsl:when>
                                </xsl:choose>

                            </equipmentStorage>
                        </lineTransaction>
                    </xsl:for-each>
                </lineTransactions>
            </warehouseQuote>
        </message>
    </xsl:template>
</xsl:stylesheet>
